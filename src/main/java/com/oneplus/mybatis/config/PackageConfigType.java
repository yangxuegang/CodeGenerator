package com.oneplus.mybatis.config;

import org.apache.commons.lang3.StringUtils;

/**
 * Title: <br/>
 * <p>
 * Create Date: 2015/11/27 23:04 <br/>
 * Package Name: com.hujifang.mybatis.config <br/>
 * Company:  All Rights Reserved. <br/>
 * Copyright © 2015 <br/>
 * </p>
 * <p>
 * author: Hujifang <br/>
 * 1st_examiner: <br/>
 * 2nd_examiner: <br/>
 * </p>
 */
public class PackageConfigType {
    /**
     * 生成目标文件夹
     */
    private String targetDir;

    /**
     * 生成文件后缀
     */
    private String fileNameSuffix=".java";

    /**
     * 生成文件模板
     */
    private String template;

    public String getTargetDir() {
        return targetDir;
    }

    public void setTargetDir(String targetDir) {
        this.targetDir = targetDir;
    }

    public String getFileNameSuffix() {
        return fileNameSuffix;
    }

    public void setFileNameSuffix(String fileNameSuffix) {
        this.fileNameSuffix =StringUtils.isNotBlank(fileNameSuffix)?fileNameSuffix:this.fileNameSuffix;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }
}
